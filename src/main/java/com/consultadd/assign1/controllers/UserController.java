package com.consultadd.assign1.controllers;

import com.consultadd.assign1.Entities.User;
import com.consultadd.assign1.repositories.UserRepository;
import com.consultadd.assign1.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService service;

    @GetMapping("")
    public List<User> getUsers() {
        return service.getUsers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") Long id) {
        return new ResponseEntity(service.getUser(id), new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/")
    public User saveUser(@RequestBody User user) {
        return service.saveUser(user);
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteUser(@PathVariable("id") Long id) {
        service.deleteUserById(id);
        return HttpStatus.OK;
    }
}
