package com.consultadd.assign1.controllers;

import com.consultadd.assign1.Entities.Post;
import com.consultadd.assign1.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("posts")
public class PostController {

    @Autowired
    PostService service;

    @GetMapping("")
    public List<Post> getPosts() {
        return service.getPosts();
    }

    @GetMapping("/{id}")
    public Post getPost(@PathVariable Long id) {
        return service.getPost(id);
    }

    @PostMapping("")
    public Post savePost(@RequestBody Post post) {
        return service.savePost(post);
    }

    @DeleteMapping("/{id}")
    public HttpStatus deletePost(@PathVariable Long id) {
        service.deletePost(id);
        return HttpStatus.OK;
    }
}
