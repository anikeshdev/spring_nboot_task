package com.consultadd.assign1.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HomeController {

    @GetMapping(path = "/")
    public String index() {
        return "{\"key\": \"value\"}";
    }
}
