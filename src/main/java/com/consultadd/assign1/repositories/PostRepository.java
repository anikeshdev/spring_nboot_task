package com.consultadd.assign1.repositories;

import com.consultadd.assign1.Entities.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository <Post, Long>{
}
