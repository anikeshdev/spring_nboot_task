package com.consultadd.assign1.services;

import com.consultadd.assign1.Entities.Post;
import com.consultadd.assign1.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {

    @Autowired
    PostRepository repository;

    public List<Post> getPosts() {
        return repository.findAll();
    }

    public Post getPost(Long id) {
        return repository.findById(id).get();
    }

    public Post savePost(Post post) {
        return repository.save(post);
    }

    public void deletePost(Long id) {
        repository.deleteById(id);
    }
}
