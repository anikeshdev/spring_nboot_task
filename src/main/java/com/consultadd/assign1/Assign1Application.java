package com.consultadd.assign1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
public class Assign1Application {

	public static void main(String[] args) {
		SpringApplication.run(Assign1Application.class, args);
	}

}
