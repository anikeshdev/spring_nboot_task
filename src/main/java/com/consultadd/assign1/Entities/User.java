package com.consultadd.assign1.Entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tb_user")
public class User{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long id;

    @Column
    public String name;

    @Column
    public String email;

    @Column
    public String password;

    @OneToMany
    @JoinColumn(name = "userId")
    public List<Post> posts;

}
