package com.consultadd.assign1.Entities;

import javax.persistence.*;

@Entity
@Table(name = "tb_post")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Column
    public String post;

    @Column
    public Long userId;
}
